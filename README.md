# Test Task

## Description

### Conditions:

a. Use GIT (GitHub) and just share the link to project with implementation

b. Project should be base on CMake

c. All interfaces should be POSIX compliant

d. Target OS: Linux

e. Language: C/C++11

f. Usage of RAII idioms (Nice to have)

### Task:
Create 2 applications:

#### Requirement#1:

First application "Parent" should create a new process "Child" and re-start the "Child" process in
case it crashes (or can be killed by SIGTERM from console).

#### Requirement#2:

During normal flow a "Child" process will increase internal counter with period from 1mS to 1S
(period should be possible to set via command line argument)

#### Requirement#3:
In case "Child" process becomes terminated, it shall be restarted by "Parent" process and the
counter should continue from the previous successful value. (The result should be clearly visible
on console with period 1S in case some printing done in stdout)

#### Output:
1) Output from console (can be stored in file) with:
- Starting “Parent” process (counting period 1 Sec);
- "Child" process log counter’s value in stdout
- "Child" process killed by SIGTERM from console and re-started by "Parent" process
(should be visible hello message during starting of "Child" )
- "Child" process should continue counting after restart
2) Link to source code in GitHub/Gitlab

## Dependencies

* Boost.Interprocess (for communications through shared memory)

## How to build

As project uses CMake so build is easy as follows:
```bash
mkdir build && cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
./parent
```

Also some test cases present in project in `tests/tests.cpp`. To run tests (from `build` folder) just:
```
./tests
# or
ctest
```

Tests built with **GoogleTest**. 

## Design considerations

To share data (internal counter) between processes **shared-memory approach** was chosen (in contrast to message passing with different mechanisms such as pipes etc). Memory available for parent and child processes and it has minimum overhead to access it.

Logging of internal counter performed in child process. Child process has 2 threads:
* increment thread;
* logging thread.

Logging thread approach is easy in implementation (because in increment thread u need 
to define complicated logic of time calculating to provide info about counter with period 1 sec). 
But logging thread also has simple race condition with increment thread so synchronization with help
of mutex added to methods for reading/writing from/to counter (but race condition is not dangerous in 
that case so synchronization can be omitted).

Parent and child processes presented as classes. Classes have `exec()` and `finish()` methods.
Also classes have static signal handler function which perform correct clean up.