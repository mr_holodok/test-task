#ifndef TEST_TASK_PARENT_PROC_HPP
#define TEST_TASK_PARENT_PROC_HPP

#include <unistd.h>

#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/shared_memory_object.hpp>

#include "child_proc.hpp"

class Parent_Proc {
 public:
  explicit Parent_Proc(int inc_period, const std::string& shmem_name);
  ~Parent_Proc();

  Parent_Proc(Parent_Proc&) = delete;
  Parent_Proc& operator=(Parent_Proc&) = delete;
  Parent_Proc(Parent_Proc&&) = delete;
  Parent_Proc& operator=(Parent_Proc&&) = delete;

  void finish();
  void finish_child() const;
  void exec();

 private:
  // handler for correct clean up and exit
  static void signal_handler(int signal);

  // static variable to hold parent process instance
  // needed for correct cleaning of object after signal handling
  // (signal_handler is static, so need this type of hack)
  static Parent_Proc* current_parent;
  const size_t shared_counter_byte_size_ = sizeof(uint64_t);

  const int inc_period_;
  const std::string& shmem_name_;
  bipc::shared_memory_object sh_counter_;

  pid_t child_pid_{0};
  volatile bool finished{false};
};

#endif  // TEST_TASK_PARENT_PROC_HPP
