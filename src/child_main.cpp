#include <cstring>
#include <iostream>

#include "child_proc.hpp"
#include "utility.hpp"

int main(int argc, char **argv) {
  // if no user input provided
  const int default_period = 1000;
  int period = default_period;

  if (argc > 1) {
    if (strcmp("--help", argv[1]) == 0) {
      Utility::print_help("child");
      return 0;
    } else {
      period = Utility::handle_period_input(argv);
      if (period == 0) {
        return 1;
      }
    }
  }

  try {
    Child_Proc child(period, Utility::shmem_name);
    child.exec();
  } catch (bipc::interprocess_exception &ex) {
    std::cerr << "Boost IPC exception caught: " << ex.what() << std::endl;
    return 1;
  }

  // child can be ended only after interruption
  return 1;
}
