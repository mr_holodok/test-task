#include "child_proc.hpp"

#include <chrono>
#include <csignal>
#include <cstring>
#include <iostream>

Child_Proc *Child_Proc::current_child{nullptr};

Child_Proc::Child_Proc(int inc_period, const std::string &shmem_name)
    : inc_period_{inc_period},
      sh_counter_{bipc::open_only, shmem_name.c_str(), bipc::read_write},
      mapped_counter_{sh_counter_, bipc::read_write, 0,
                      shared_counter_byte_size_} {
  std::cout << "[LOG] Child process started!" << std::endl;

  // separate thread for logging with period of 1 sec
  log_thread_ = std::thread([this]() {
    while (!finished_) {
      auto value = get_shared_counter_value();
      std::cout << "[LOG] Value of counter is " << value << std::endl;

      auto log_period = std::chrono::seconds(1);
      std::this_thread::sleep_for(log_period);
    }
  });

  Child_Proc::current_child = this;

  // redefine signal handlers from parent process
  std::signal(SIGTERM, signal_handler);
  std::signal(SIGINT, signal_handler);
  std::signal(SIGABRT, signal_handler);
  std::signal(SIGQUIT, signal_handler);
}

void Child_Proc::exec() {
  while (!finished_) {
    auto value = get_shared_counter_value();

    if (value == std::numeric_limits<uint64_t>::max()) {
      std::cerr << "[ERROR] Counter exceeded it\'s maximum! Resetting to zero!"
                << std::endl;
    }

    value++;
    set_shared_counter_value(value);
    std::this_thread::sleep_for(std::chrono::milliseconds(inc_period_));
  }
}

uint64_t Child_Proc::get_shared_counter_value() const {
  // lock_guard used to avoid race condition between log thread and
  // increment thread; but possible race condition is not dangerous
  // so synchronization can be omitted
  std::lock_guard<std::mutex> lg(mu_);
  return *(static_cast<uint64_t *>(mapped_counter_.get_address()));
}

void Child_Proc::set_shared_counter_value(const uint64_t &value) {
  // lock_guard used to avoid race condition between log thread and
  // increment thread; but possible race condition is not dangerous
  // so synchronization can be omitted
  std::lock_guard<std::mutex> lg(mu_);
  std::memcpy(mapped_counter_.get_address(), &value, shared_counter_byte_size_);
}

Child_Proc::~Child_Proc() {
  // providing correct thread clean up
  if (log_thread_.joinable()) {
    log_thread_.join();
  }
  Child_Proc::current_child = nullptr;
}

void Child_Proc::finish() {
  // will end loops in log thread and in 'exec()' function
  finished_ = true;
}

void Child_Proc::signal_handler(int signal) {
  if (Child_Proc::current_child) {
    Child_Proc::current_child->finish();
  }
}
