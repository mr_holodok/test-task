#ifndef TEST_TASK_UTILITY_HPP
#define TEST_TASK_UTILITY_HPP

namespace Utility {

static const std::string shmem_name = "Shared_Memory_Parent_Child";

void print_help(const std::string &prog_name) {
  std::cout << "Usage: " << prog_name << " [period / --help] " << std::endl
            << "* [period] is increment time in milliseconds and can be from 1 "
               "to 1000, "
            << std::endl
            << "         default value is 1000" << std::endl
            << "* [--help] display this message" << std::endl;
}

int handle_period_input(char **argv) {
  try {
    int period = std::stoi(argv[1]);
    if (period < 1 || period > 1000) {
      std::cout << "Value of \'period\' parameter is out of expected range! "
                   "Should be from 1 to 1000."
                << std::endl;
      return 0;
    } else {
      return period;
    }
  } catch (std::invalid_argument &ex) {
    std::cout
        << "Wrong \'period\' parameter value! Should be integer from 1 to 1000."
        << std::endl;
    return 0;
  } catch (std::out_of_range &ex) {
    std::cout
        << "Value of \'period\' parameter is too big! Should be from 1 to 1000."
        << std::endl;
    return 0;
  }
}

}  // namespace Utility

#endif  // TEST_TASK_UTILITY_HPP
