#include "parent_proc.hpp"

#include <sys/wait.h>
#include <unistd.h>

#include <csignal>
#include <iostream>

Parent_Proc* Parent_Proc::current_parent{nullptr};

void Parent_Proc::exec() {
  do {
    child_pid_ = fork();

    if (child_pid_ < 0) {
      std::cerr << "[ERROR] Fork failed!" << std::endl;
      finish();
    } else if (child_pid_ == 0) {
      char period[5 * sizeof(char)];
      std::sprintf(period, "%d", inc_period_);

      char* args[] = {"./child", period, nullptr};
      execv(args[0], args);

      // exiting from child process with 1 cause it can be exited only on
      // interruption and exiting here to prevent cleaning of 'parent' object
      exit(1);
    } else {
      // waiting for child to die ...
      if (wait(nullptr) == -1) {
        std::cerr << "[ERROR] An error occured when trying to wait for child!"
                  << std::endl;
        finish();
      }
    }
  } while (!finished);

  // execution ended
}

Parent_Proc::Parent_Proc(int inc_period, const std::string& shmem_name)
    : inc_period_{inc_period},
      shmem_name_{shmem_name},
      sh_counter_{bipc::create_only, shmem_name.c_str(), bipc::read_write} {
  std::cout << "[LOG] Parent process started!" << std::endl;
  sh_counter_.truncate(shared_counter_byte_size_);
  current_parent = this;

  std::signal(SIGTERM, signal_handler);
  std::signal(SIGINT, signal_handler);
  std::signal(SIGABRT, signal_handler);
  std::signal(SIGQUIT, signal_handler);
}

Parent_Proc::~Parent_Proc() {
  bool success = bipc::shared_memory_object::remove(shmem_name_.c_str());
  if (!success) {
    std::cerr << "[ERROR] Failed to remove shared memory object!" << std::endl;
  }
  current_parent = nullptr;
}

void Parent_Proc::signal_handler(int signal) {
  if (Parent_Proc::current_parent != nullptr) {
    // as parent proc unmaps shared memory, so firstly child process
    // should be finished; after that loop in 'exec()' ends and
    // execution
    Parent_Proc::current_parent->finish_child();
    Parent_Proc::current_parent->finish();
  }
}

void Parent_Proc::finish() { finished = true; }

void Parent_Proc::finish_child() const {
  if (child_pid_ != 0) {
    // send SIGTERM to child and force him to die
    kill(child_pid_, SIGTERM);
  }
}
