#ifndef TEST_TASK_CHILD_PROC_H
#define TEST_TASK_CHILD_PROC_H

#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <mutex>
#include <thread>

namespace bipc = boost::interprocess;

class Child_Proc {
 public:
  Child_Proc(int inc_period, const std::string& shmem_name);
  ~Child_Proc();

  Child_Proc(Child_Proc&) = delete;
  Child_Proc& operator=(Child_Proc&) = delete;
  Child_Proc(Child_Proc&&) = delete;
  Child_Proc& operator=(Child_Proc&&) = delete;

  void exec();
  void finish();

 private:
  uint64_t get_shared_counter_value() const;
  void set_shared_counter_value(const uint64_t& value);

  // handler for correct clean up and exit
  static void signal_handler(int signal);

  // static variable to hold child process instance
  // needed for correct cleaning of object after signal handling
  // (signal_handler is static, so need this type of hack)
  static Child_Proc* current_child;
  const size_t shared_counter_byte_size_ = sizeof(uint64_t);

  int inc_period_;
  bipc::shared_memory_object sh_counter_;
  bipc::mapped_region mapped_counter_;

  std::thread log_thread_;
  // mutex to avoid race condition
  mutable std::mutex mu_;
  volatile bool finished_{false};
};

#endif  // TEST_TASK_CHILD_PROC_H
