#include <cstdlib>
#include <fstream>
#include <iostream>

#include "../src/parent_proc.hpp"
#include "gtest/gtest.h"

std::string get_process_pids(const std::string& proc_name) {
  std::string cmd = "ps aux | grep -v grep | grep ./" + proc_name +
                    " | awk '{print $2}' > file_for_test.txt";
  std::system(cmd.c_str());

  std::stringstream res;
  res << std::ifstream("file_for_test.txt").rdbuf();
  return res.str();
}

TEST(Test, Test_Create_And_Kill) {
  std::system("./parent > out.txt &");

  std::this_thread::sleep_for(std::chrono::seconds(2));

  // TEST THAT PARENT PROCESS WAS CREATED
  auto res_str = get_process_pids("parent");
  int processes_count = 0;
  int occurence = -1;
  do {
    occurence = res_str.find('\n', occurence + 1);
  } while (static_cast<ulong>(occurence) != std::string::npos &&
           ++processes_count);

  EXPECT_EQ(processes_count, 1);

  auto parend_id = std::atoi(res_str.substr(0, res_str.find('\n')).c_str());

  // TEST THAT CHILD PROCESS WAS CREATED
  res_str = get_process_pids("child");
  processes_count = 0;
  occurence = -1;
  do {
    occurence = res_str.find('\n', occurence + 1);
  } while (static_cast<ulong>(occurence) != std::string::npos &&
           ++processes_count);

  EXPECT_EQ(processes_count, 1);

  // KILL THEM AND CHECK THAT IT WORKED

  kill(parend_id, SIGTERM);

  std::this_thread::sleep_for(std::chrono::seconds(2));

  res_str = get_process_pids("child");
  EXPECT_TRUE(res_str.empty());

  res_str = get_process_pids("parent");
  EXPECT_TRUE(res_str.empty());
}

TEST(Test, Test_Restart) {
  std::system("./parent > out.txt &");

  std::this_thread::sleep_for(std::chrono::seconds(2));

  // GET 1ST CHILD PID
  auto res_str = get_process_pids("child");
  auto index = res_str.find('\n');
  EXPECT_NE(index, std::string::npos);

  auto prev_child_id = std::atoi(res_str.substr(0, index).c_str());

  // KILLING 1ST CHILD
  kill(prev_child_id, SIGTERM);

  std::this_thread::sleep_for(std::chrono::seconds(2));

  // GET 2ND CHILD PID
  res_str = get_process_pids("child");
  index = res_str.find('\n');
  EXPECT_NE(index, std::string::npos);

  auto new_child_id = std::atoi(res_str.substr(0, index).c_str());

  EXPECT_NE(prev_child_id, new_child_id);

  // KILL ALL (FROM PARENT)
  res_str = get_process_pids("parent");
  index = res_str.find('\n');
  auto parent_id = std::atoi(res_str.substr(0, index).c_str());
  kill(parent_id, SIGTERM);
}

TEST(Test, Test_Counter_Continue) {
  // ASSUMING THAT PREV TEST SUCCESSFULLY EXECUTED AND OUTPUT IN 'OUT.TXT' FILE
  std::stringstream res;
  res << std::ifstream("out.txt").rdbuf();
  auto res_str = res.str();
  std::string creation_str = "[LOG] Child process started!";
  // first child process created
  int index1 = res_str.find(creation_str);
  // second child process created
  int index2 = res_str.find(creation_str, index1 + 1);

  int index_prev_counter_start =
      res_str.rfind(' ', static_cast<ulong>(index2)) + 1;
  int prev_counter_val = std::atoi(
      res_str
          .substr(index_prev_counter_start, index2 - index_prev_counter_start)
          .c_str());

  int index_next_counter_end = res_str.find(
      '\n', static_cast<ulong>(index2 + creation_str.length() + 1));
  int index_next_counter_start =
      res_str.rfind(' ', static_cast<ulong>(index_next_counter_end)) + 1;
  int next_counter_val =
      std::atoi(res_str
                    .substr(index_next_counter_start,
                            index_next_counter_end - index_next_counter_start)
                    .c_str());

  std::cout
      << "Counter values before and after new child process creation are:\n"
      << prev_counter_val << " " << next_counter_val << std::endl;
  EXPECT_GT(next_counter_val, prev_counter_val);
}
